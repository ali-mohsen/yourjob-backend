<?php

use App\Http\Controllers\CountryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth:sanctum'], function (){
    //user controller
    Route::post('update-this-account', [UserController::class, 'updateUser']);
    Route::post('delete-this-account', [UserController::class, 'deleteUser']);
    Route::post('add-language', [UserController::class, 'addLanguage']);
    Route::post('add-course', [UserController::class, 'addCourse']);
    Route::post('delete-language', [UserController::class, 'deleteLanguage']);
    Route::post('delete-course', [UserController::class, 'deleteCourse']);
    Route::post('update-user-tags', [UserController::class, 'updateUserTags']);
    Route::post('review-user-by-id', [UserController::class, 'reviewUserById']);
    Route::post('update-user-categories', [UserController::class, 'updateUserCategories']);
    Route::post('buy-package', [UserController::class, 'buyPackage']);
    Route::post('apply', [UserController::class, 'apply']);
    Route::post('send-message', [UserController::class, 'sendMessage']);
    Route::post('receive-all-messages', [UserController::class, 'receiveAllMessages']);
    Route::post('add-post-to-favorite', [UserController::class, 'addPostToFavorite']);
    Route::post('delete-post-from-favorite', [UserController::class, 'deletePostFromFavorite']);
    Route::post('notifications', [UserController::class, 'notifications']);
    Route::post('is-user-accepted-in-post', [UserController::class, 'isUserAcceptedInPost']);
    Route::post('user-applied-to-post', [UserController::class, 'UserAppliedToPost']);


    //dashboard
    Route::post('delete-account-by-id', [UserController::class, 'deleteUserById']);
    Route::get('get-users', 'App\Http\Controllers\UserController@getAllUsers');
    Route::post('get-user-by-id', 'App\Http\Controllers\UserController@getUserById');
    Route::post('add-money-to-user', [DashboardController::class, 'addMoneyToUser']);
    Route::post('create-tag-or-category', [TagController::class, 'createTagOrCategory']);
    Route::post('get-all-reports', [DashboardController::class, 'getAllReports']);
    Route::post('make-report-seen', [DashboardController::class, 'makeReportAsSeen']);
    Route::post('add-new-package', [DashboardController::class, 'addNewpackage']);
    Route::post('get-unaccepted-posts', [DashboardController::class, 'getUnacceptedPosts']);
    Route::post('get-modified-posts', [DashboardController::class, 'getModifiedPosts']);
    Route::post('accept-post', [DashboardController::class, 'acceptPost']);
    Route::post('get-unvalidated-companies', [DashboardController::class, 'getUnvalidatedCompanies']);
    Route::post('validate-company', [DashboardController::class, 'validateCompany']);


    //post controller
    Route::post('create-post', [PostController::class, 'createPost']);
    Route::post('update-post-by-id', [PostController::class, 'updatePostById']);
    Route::post('delete-post-by-id', [PostController::class, 'deletePostById']);
    Route::post('stop-post-by-id', [PostController::class, 'stopPostById']);
    Route::post('report-post-by-id', [PostController::class, 'reportPostById']);
    Route::post('rate-post-by-id', [PostController::class, 'ratePostById']);
    Route::post('get-applicants', [PostController::class, 'getApplicants']);
    Route::post('accept-applicant', [PostController::class, 'acceptApplicant']);
    Route::post('refuse-applicant', [PostController::class, 'refuseApplicant']);
    Route::post('get-favorites', [PostController::class, 'getFavorites']);
    Route::post('get-user-posts', [PostController::class, 'getUserPosts']);
    Route::post('my-jobs', [PostController::class, 'myJobs']);

});


//user
Route::post('register', [UserController::class, 'register']);
Route::post('register-admin', [UserController::class, 'registerAdmin']);
Route::post('login', [UserController::class, 'login']);
Route::post('find_email', [UserController::class, 'findEmail']);


//helping APIs
Route::post('get-all-packages', [UserController::class, 'getAllPackages']);
Route::post('get-all-posts', [PostController::class, 'getAllPosts']);
Route::post('suggest-tags-or-categories', [TagController::class, 'suggestTagsOrCategories']);
Route::post('search', [UserController::class, 'search']);


//country controller
Route::post('get-all-countries', [CountryController::class, 'getAllCountries']);
Route::post('get-all-cities', [CountryController::class, 'getAllCities']);
Route::post('get-cities-in-country', [CountryController::class, 'getCountryCities']);
Route::post('find-countries', [CountryController::class, 'searchCountryByName']);
Route::post('find-cities-in-country', [CountryController::class, 'searchCityByNameAndCountry']);


//still_have_posts
// in login