<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_details', function (Blueprint $table){
            $table->id();
            $table->string('institute_name');
            $table->foreignId('user_id')->unique()->constrained();
            $table->foreignId('institute_type_id')->constrained('institute_types');
         
      
            $table->string('institute_field');
            
            $table->text('about')->nullable();
            $table->foreignId('number_of_employees')->constrained('number_of_employees');
            
            //it will  be verified using the dashboard by the admin.
            $table->boolean('verified')->default(false);
            //link to the verfication image 
            $table->string('verfication')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyDetails');
    }
}
