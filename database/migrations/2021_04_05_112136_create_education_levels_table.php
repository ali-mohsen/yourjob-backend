<?php

use Illuminate\support\facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_levels', function (Blueprint $table) {
            $table->id();
            $table->string('education_level');
            //$table->timestamps();
        });
        DB::table('education_levels')->insert(['education_level' => 'Less than High school']);
        DB::table('education_levels')->insert(['education_level' => 'High school']);
        DB::table('education_levels')->insert(['education_level' => 'Diploma']);
        DB::table('education_levels')->insert(['education_level' => 'University']);
        DB::table('education_levels')->insert(['education_level' => 'Graduated']);
        DB::table('education_levels')->insert(['education_level' => 'Master']);
        DB::table('education_levels')->insert(['education_level' => 'Doctorate']);
        //DB::table('education_levels')->insert(['education_level' => 'other']);
    }
/*




 


*/
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_levels');
    }
}
