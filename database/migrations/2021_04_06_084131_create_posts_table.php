<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('type_id')->constrained('post_types');

            $table->string('title');
            $table->boolean('is_waiting')->default(true);
            $table->text('about');
            //سترينغ لان الراتب رينج 
            $table->string('money');
            $table->string('image')->nullable();
            $table->boolean('modified')->default(false);
            $table->boolean('accepted')->default(false);
            //ALTER TABLE `posts` ADD COLUMN accepted tinyint(1) DEFAULT 0 NOT NULL AFTER deleted
            $table->boolean('deleted')->default(false);

            //only for job
            $table->text('required_experience')->nullable();
            $table->enum('employment_type', [
                'Full time',
                'Part time',
                'Contract',
                'Remotely'
                ])->nullable();
            $table->foreignId('location')->nullable()->constrained('cities');

            //only for freelance

            //only for service
            $table->string('servise_time')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
