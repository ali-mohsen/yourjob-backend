<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table){
           $table->id();
           $table->string('first_name');
           $table->string('last_name');
           $table->foreignId('user_id')->unique()->constrained();
           $table->enum('gender', ['male', 'female']);
           $table->year('birth_year')->nullable();
           //$table->date('birth_date');
           
           //$table->foreignId('career_level_id')->constrained('career_levels');
           $table->foreignId('education_level_id')->constrained('education_levels');
           
           //بدال هاد عنا التاغز يلي نوعها كاتيغوريي لليوزر (تصنيفات اليوزر)
           //$table->string('work_field')->nullable();
           
           $table->string('main_profession')->nullable();
           $table->text('work_experience')->nullable();
           $table->string('education')->nullable();
           $table->string('skills')->nullable();

           //courses and languages are  outside table
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userDetails');
    }
}
