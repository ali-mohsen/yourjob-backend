<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateNumberOfEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('number_of_employees', function (Blueprint $table) {
            $table->id();
            $table->string('num_of_emp');
            $table->timestamps();
        });
        DB::table('number_of_employees')->insert(['num_of_emp' => '1-10']);
        DB::table('number_of_employees')->insert(['num_of_emp' => '11-50']);
        DB::table('number_of_employees')->insert(['num_of_emp' => '51-200']);
        DB::table('number_of_employees')->insert(['num_of_emp' => '200-500']);
        DB::table('number_of_employees')->insert(['num_of_emp' => '500-1000']);
        DB::table('number_of_employees')->insert(['num_of_emp' => '+1000']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('number_of_employees');
    }
}
