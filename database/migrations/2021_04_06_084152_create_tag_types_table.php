<?php

use Illuminate\support\facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateTagTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_types', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->timestamps();
        });


        // DB::table('tag_types')->insert(['type' => 'post-tag']);
        // DB::table('tag_types')->insert(['type' => 'company-tag']);
        // DB::table('tag_types')->insert(['type' => 'normal-user-tag']);
        DB::table('tag_types')->insert(['type' => 'tag']);
        DB::table('tag_types')->insert(['type' => 'category']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_types');
    }
}
