<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('tag');
            $table->boolean('show_in_suggestions')->default(false);
            $table->foreignId('tag_type_id')->default(1)->constrained('tag_types');
            $table->timestamps();
        });

        //tags
        DB::table('tags')->insert([
            'tag' => 'Java',
            'show_in_suggestions' => true,
            'tag_type_id' => 1
            ]);
        DB::table('tags')->insert([
            'tag' => 'Android',
            'show_in_suggestions' => true,
            'tag_type_id' => 1
            ]);

        //categories
        DB::table('tags')->insert([
            'tag' => 'Programming and development',
            'show_in_suggestions' => true,
            'tag_type_id' => 2
            ]);
        DB::table('tags')->insert([
            'tag' => 'Design',
            'show_in_suggestions' => true,
            'tag_type_id' => 2
            ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
