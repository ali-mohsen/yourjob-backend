<?php

use Illuminate\support\facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstituteTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institute_types', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            //$table->timestamps();
        });
        DB::table('institute_types')->insert(['type' => 'Company']);
        DB::table('institute_types')->insert(['type' => 'Educational institution']);
        DB::table('institute_types')->insert(['type' => 'University']);
        DB::table('institute_types')->insert(['type' => 'Clinic']);
        DB::table('institute_types')->insert(['type' => 'Governmental institution']);
        DB::table('institute_types')->insert(['type' => 'Gym']);
        DB::table('institute_types')->insert(['type' => 'Factory']);
        DB::table('institute_types')->insert(['type' => 'Bank']);
        DB::table('institute_types')->insert(['type' => 'Charity']);
        DB::table('institute_types')->insert(['type' => 'Store']);
        DB::table('institute_types')->insert(['type' => '-']);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institute_types');
    }
}
