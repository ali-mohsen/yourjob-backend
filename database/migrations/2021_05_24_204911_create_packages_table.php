<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            //$table->string('name');
            $table->enum('package_for', ['normal user', 'company']);
            $table->double('price');
            $table->integer('posts_number');
            $table->enum('availble_for', ['day', 'month', 'year'])->default('month');
            //$table->timestamps();
        });
        DB::table('packages')->insert([
            'package_for' => 'normal user',
            'price' => 5000,
            'posts_number' => 5,
            'availble_for' => 'month'
            ]);
        DB::table('packages')->insert([
            'package_for' => 'company',
            'price' => 3000,
            'posts_number' => 3
            ]);
        DB::table('packages')->insert([
            'package_for' => 'normal user',
            'price' => 10000,
            'posts_number' => 10
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
