<?php

namespace App\Http\Controllers;

use App\Models\Account_type;
use App\Models\Applicant;
use App\Models\City;
use App\Models\Company_details;
use App\Models\Country;
use App\Models\Institute_type;
use App\Models\Number_of_employees;
use App\Models\Package;
use App\Models\Post;
use App\Models\Post_rating;
use App\Models\Post_type;
use App\Models\User;
use App\Models\Report;
use App\Models\Review;
use App\Models\User_details;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;

class DashboardController extends Controller
{   
    public function addMoneyToUser(Request $request)
    {
        $request->validate([
            'money' => ['required', 'numeric'],
            'user_email' => ['required', 'email', 'exists:users,email'] 
        ]);

        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission to add money to users'
            ], 403);
        }

        $user =  User::where('email', $request->user_email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'user is not found'
            ]);
        }

        $user->account_money += $request->money;
        $user->save();

        return response()->json([
            'message' =>  "done, user account have: ". $user->account_money
        ]);
    }

    public function getAllReports(Request $request)
    {
        $request->validate([
            'post_id' => ['exists:posts,id'],
        ]);
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        if ($request->has('post_id')) {
            $reports = Report::where('seen_by_admin', 0)->where('post_id', $request->post_id)->get();
        }else {
            $reports = Report::where('seen_by_admin', 0)->get();
        }
        foreach ($reports as $key => $value) {
            $post = Post::find($value->post_id);
            if ($post->deleted) {
                unset($reports[$key]);
                continue;
            }
            $arr = TagController::getPostTagsAndCategories($post->id);
            $post['tags'] = $arr['tags'];
            $post['categories'] = $arr['categories'];
            $ratings = Post_rating::where('post_id', $post->id)->get();
            $post['ratings'] = $ratings;
            $city = City::find($post->location);
            if ($city) {
                $post['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $post['country'] = $country['name'];
            }
            $postType = Post_type::find($post->type_id);
            $post['post_type'] = $postType['type'];
            $applicantsNumber = Applicant::where('post_id', $post->id)->get()->count();
            $post['number_of_applicants'] = $applicantsNumber;
            $post['total_rating'] = PostController::getPostTotalRating($post->id);
            if ($post->image) {
                $post['image'] = URL::to('/') . "/images/posts/$post->image";
            }
            $user = User::find($post->user_id);
            $userArr = [];
            $userType = Account_type::find($user->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($user->image) {
                $userArr['image'] = URL::to('/') . "/images/profile/$user->image";
            }
            $post['user'] = $userArr;
            $reports[$key]['post'] = $post;
        }
        return response()->json([
            'reports' => $reports
        ]);
    }
    public function makeReportAsSeen(Request $request)
    {
        $request->validate([
            'report_id' => ['required', 'exists:reports,id'],
        ]);
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        $report = Report::find($request->report_id);
        $report->seen_by_admin = true;
        $report->save();
        return response()->json([
            'message' => 'done'
        ]);

    }
    public function addNewpackage(Request $request)
    {
        $request->validate([
            'package_for' => ['required', Rule::in(['normal user', 'company'])],
            'price' => ['required', 'numeric'],
            'posts_number' => ['required', 'numeric'],
            'availble_for' => ['required', Rule::in(['day', 'month', 'year'])]
        ]);
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        $package = Package::create([
            'package_for' => $request->package_for,
            'price' => $request->price,
            'posts_number' => $request->posts_number,
            'availble_for' => $request->availble_for
        ]);
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function getUnacceptedPosts(Request $request)
    {
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        $posts = Post::where('accepted', false)
        ->where('modified', false)
        ->where('deleted', false)->get();
        foreach ($posts as $key => $value) {
            $arr = TagController::getPostTagsAndCategories($value->id);
            $posts[$key]['tags'] = $arr['tags'];
            $posts[$key]['categories'] = $arr['categories'];
            $ratings = Post_rating::where('post_id', $value->id)->get();
            $posts[$key]['ratings'] = $ratings;
            if ($value->image) {
                $posts[$key]['image'] = URL::to('/') . "/images/posts/$value->image";
            }
            $city = City::find($value->location);
            if ($city) {
                $posts[$key]['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $posts[$key]['country'] = $country['name'];
            }
            $postType = Post_type::find($value->type_id);
            $posts[$key]['post_type'] = $postType['type'];
            $user = User::find($value->user_id);
            $userArr = [];
            $userType = Account_type::find($user->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($user->image) {
                $userArr['image'] = URL::to('/') . "/images/profile/$user->image";
            }
            $posts[$key]['user'] = $userArr;
        }
        return response()->json([
            'posts' => $posts
        ]);
    }
    public function getModifiedPosts(Request $request)
    {
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        $posts = Post::where('accepted', false)
        ->where('modified', true)
        ->where('deleted', false)->get();
        foreach ($posts as $key => $value) {
            $arr = TagController::getPostTagsAndCategories($value->id);
            $posts[$key]['tags'] = $arr['tags'];
            $posts[$key]['categories'] = $arr['categories'];
            $ratings = Post_rating::where('post_id', $value->id)->get();
            $posts[$key]['ratings'] = $ratings;
            if ($value->image) {
                $posts[$key]['image'] = URL::to('/') . "/images/posts/$value->image";
            }
            $city = City::find($value->location);
            if ($city) {
                $returnPosts[$key]['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $returnPosts[$key]['country'] = $country['name']; 
            }
            $postType = Post_type::find($value->type_id);
            $posts[$key]['post_type'] = $postType['type'];
            $user = User::find($value->user_id);
            $userArr = [];
            $userType = Account_type::find($user->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($user->image) {
                $userArr['image'] = URL::to('/') . "/images/profile/$user->image";
            }
            $posts[$key]['user'] = $userArr;
        }
        return response()->json([
            'posts' => $posts
        ]);
    }
    public function acceptPost(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
        ]);
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        $post = Post::find($request->post_id);
        $post->accepted = true;
        $post->save();
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function getUnvalidatedCompanies(Request $request)
    {
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        $companyType = Account_type::where('type', 'company')->first();
        $companies = User::where('deleted', false)->where('account_type_id', $companyType->id)->get();
        foreach ($companies as $key => $value) {
            $details = Company_details::where('user_id', $value->id)->first();
            if ($details->verified) {
                unset($companies[$key]);
                continue;
            }
            $instituteType = Institute_type::find($details->institute_type_id);
            $details->institute_type = $instituteType->type;
            $numberOfEmployees = Number_of_employees::find($details->number_of_employees);
            $details->number_of_employees = $numberOfEmployees->num_of_emp;
            if ($details->verfication) {
                $details->verfication = URL::to('/') . "/images/verification/$details->verfication";
            }
            $companies[$key]['user_type'] = 'company';
            $companies[$key]['details'] = $details;
            $arr = TagController::getUserTagsAndCategories($value->id);
            $companies[$key]['tags'] = $arr['tags'];
            $companies[$key]['categories'] = $arr['categories'];
            $reviews = Review::where('user_2_id', $value->id)->get(['user_1_id as reviewer_id', 'review']);
            $companies[$key]['reviews'] = $reviews;
            if ($value->image) {
                // $image = public_path('/images/profile/') . $value->image;
                $companies[$key]['image'] = URL::to('/') . "/images/profile/$value->image";
            }
            if ($value->city_id) {
                $city = City::find($value->city_id);
                $companies[$key]['city'] = $city['city'];
            }
            if ($value->country_id) {
                $country = Country::find($value->country_id);
                $companies[$key]['country'] = $country['name'];
            }
        }
        return response()->json([
            'unvalidated_companies' => $companies
        ], 200);
    }
    public function validateCompany(Request $request)
    {
        $request->validate([
            'user_id' => ['required', 'exists:users,id'],
        ]);
        $admin = $request->user();
        if (!$admin->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission'
            ], 403);
        }
        $companyType = Account_type::where('type', 'company')->first();
        $company = User::find($request->user_id);
        if ($company->deleted) {
            return response()->json([
                'message' => 'this user is deleted'
            ], 406);
        }
        if ($company->account_type_id != $companyType->id) {
            return response()->json([
                'message' => 'this is not a company'
            ], 406);
        }
        $details = Company_details::where('user_id', $request->user_id)->first();
        $details->verified = true;
        $details->save();
        return response()->json([
            'message' => 'done'
        ]);
    }

}
