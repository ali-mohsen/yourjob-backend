<?php

namespace App\Http\Controllers;

use App\Models\Account_type;
use App\Models\Applicant;
use App\Models\City;
use App\Models\Company_details;
use App\Models\Country;
use App\Models\Favorite;
use App\Models\Package;
use App\Models\Post;
use App\Models\Post_rating;
use App\Models\Report;
use App\Models\Post_type;
use App\Models\User;
use App\Models\User_details;
use App\Models\User_package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    public function getAllPosts(Request $request)
    {
        $request->validate([
            'employment_type' => [
                Rule::in([
                    'Full time',
                    'Part time',
                    'Contract',
                    'Remotely'
                ]),
            ],
            'salary_from' => ['numeric'],
            'salary_to' => ['numeric'],
            'country_id' => 'exists:countries,id',
            'city_id' => 'exists:cities,id',
            'categories' => 'array'
        ]);
        // TODO: this is just for testing
        // $posts = Post::where('deleted', false)->where('accepted', true)->where('is_waiting', true);
        $posts = Post::where('deleted', false)->where('is_waiting', true);
        if ($request->has('employment_type')) {
            $posts->where('employment_type', $request->employment_type);
        }
        if ($request->has('country_id')) {
            $country = Country::find($request->country_id);
            $cities = City::where('iso2', $country->code)->get('id');
            foreach ($cities as $key => $value) {
                $cities[$key] = $value->id;
            }
            $posts->whereIn('location', $cities);
        }
        if ($request->has('city_id')) {
            $posts->where('location', $request->city_id);
        }
        $returnPosts = $posts->orderBy('created_at', 'DESC')->get();
        $returnObj = [];
        foreach ($returnPosts as $key => $value) {
            $postType = Post_type::find($value->type_id);
            $returnPosts[$key]['post_type'] = $postType['type'];
            if ($postType['type'] == 'service') {
                if ($request->has('salary_from')) {
                    if ($request->salary_from > $value->money) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
                if ($request->has('salary_to')) {
                    if ($request->salary_to < $value->money) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
            }else{
                $money = $value->money;
                $money = explode('-', $money);
                $salaryFrom = $money[0];
                $salaryTo = $money[1];
                if ($request->has('salary_from')) {
                    if ($request->salary_from > $salaryFrom) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
                if ($request->has('salary_to')) {
                    if ($request->salary_to < $salaryTo) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
            }
            $returnPosts[$key]['total_rating'] = self::getPostTotalRating($value->id);
            $arr = TagController::getPostTagsAndCategories($value->id);
            $returnPosts[$key]['tags'] = $arr['tags'];
            $returnPosts[$key]['categories'] = $arr['categories'];
            if ($request->has('categories') && !empty($request->categories)) {
                $diff = array_diff($arr['categories'],$request->categories);
                //مافي ولا شي مشترك بين المصفوفتين
                if(count($diff) == count($arr['categories'])){
                    unset($returnPosts[$key]);
                    continue;
                }
            }
            $user = User::find($value->user_id);
            $userArr = [];
            $userType = Account_type::find($user->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($user->image) {
                // $image = public_path('/images/profile/') . $user->image;
                $userArr['image'] = URL::to('/') . "/images/profile/$user->image";
            }
            $returnPosts[$key]['user'] = $userArr;
            $ratings = Post_rating::where('post_id', $value->id)->get();
            $returnPosts[$key]['ratings'] = $ratings;
            if ($value->image) {
                $returnPosts[$key]['image'] = URL::to('/') . "/images/posts/$value->image";
            }
            $city = City::find($value->location);
            if ($city) {
                $returnPosts[$key]['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $returnPosts[$key]['country'] = $country['name']; 
            }
            $applicantsNumber = Applicant::where('post_id', $value->id)->get()->count();
            $returnPosts[$key]['number_of_applicants'] = $applicantsNumber; 
            $returnObj[] = $returnPosts[$key];
        }
        return response()->json([
            'posts' => $returnObj
        ],200);
    }
    public function myJobs(Request $request)
    {
        $request->validate([
            'employment_type' => [
                Rule::in([
                    'Full time',
                    'Part time',
                    'Contract',
                    'Remotely'
                ]),
            ],
            'salary_from' => ['numeric'],
            'salary_to' => ['numeric'],
            'country_id' => 'exists:countries,id',
            'city_id' => 'exists:cities,id',
            'categories' => 'array'
        ]);
        // TODO: this is just for testing
        // $posts = Post::where('deleted', false)->where('accepted', true)->where('is_waiting', true);
        $posts = Post::where('deleted', false)->where('is_waiting', true);
        if ($request->has('employment_type')) {
            $posts->where('employment_type', $request->employment_type);
        }
        if ($request->has('country_id')) {
            $country = Country::find($request->country_id);
            $cities = City::where('iso2', $country->code)->get('id');
            foreach ($cities as $key => $value) {
                $cities[$key] = $value->id;
            }
            $posts->whereIn('location', $cities);
        }
        if ($request->has('city_id')) {
            $posts->where('location', $request->city_id);
        }
        $returnPosts = $posts->orderBy('created_at', 'DESC')->get();
        $returnObj = [];
        $returnPosts[$key]['total_rating'] = self::getPostTotalRating($value->id);

        $user_id = $request->user();
        $user_id = $user_id->id;
        $userArr = TagController::getUserTagsAndCategories($user_id);
        $userTags = $userArr['tags'];
        $userCategories = $userArr['categories'];

        foreach ($returnPosts as $key => $value) {
            $arr = TagController::getPostTagsAndCategories($value->id);
            $returnPosts[$key]['tags'] = $arr['tags'];
            $returnPosts[$key]['categories'] = $arr['categories'];
            if ($userCategories  && !empty($userCategories)) {
                $diff = array_diff($arr['categories'],$userCategories);
                //مافي ولا شي مشترك بين المصفوفتين
                if(count($diff) == count($arr['categories'])){
                    unset($returnPosts[$key]);
                    continue;
                }
            }
            if ($userTags  && !empty($userTags)) {
                $diff = array_diff($arr['tags'],$userTags);
                //مافي ولا شي مشترك بين المصفوفتين
                if(count($diff) == count($arr['tags'])){
                    unset($returnPosts[$key]);
                    continue;
                }
            }
            if ($request->has('categories') && !empty($request->categories)) {
                $diff = array_diff($arr['categories'],$request->categories);
                //مافي ولا شي مشترك بين المصفوفتين
                if(count($diff) == count($arr['categories'])){
                    unset($returnPosts[$key]);
                    continue;
                }
            }
            $postType = Post_type::find($value->type_id);
            $returnPosts[$key]['post_type'] = $postType['type'];
            if ($postType['type'] == 'service') {
                if ($request->has('salary_from')) {
                    if ($request->salary_from > $value->money) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
                if ($request->has('salary_to')) {
                    if ($request->salary_to < $value->money) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
            }else{
                $money = $value->money;
                $money = explode('-', $money);
                $salaryFrom = $money[0];
                $salaryTo = $money[1];
                if ($request->has('salary_from')) {
                    if ($request->salary_from > $salaryFrom) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
                if ($request->has('salary_to')) {
                    if ($request->salary_to < $salaryTo) {
                        unset($returnPosts[$key]);
                        continue;
                    }
                }
            }
            $user = User::find($value->user_id);
            $userArr = [];
            $userType = Account_type::find($user->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($user->image) {
                // $image = public_path('/images/profile/') . $user->image;
                $userArr['image'] = URL::to('/') . "/images/profile/$user->image";
            }
            $returnPosts[$key]['user'] = $userArr;
            $ratings = Post_rating::where('post_id', $value->id)->get();
            $returnPosts[$key]['ratings'] = $ratings;
            if ($value->image) {
                // $image = public_path('/images/posts/') . $value->image;
                $returnPosts[$key]['image'] = URL::to('/') . "/images/posts/$value->image";
            }
            $city = City::find($value->location);
            if ($city) {
                $returnPosts[$key]['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $returnPosts[$key]['country'] = $country['name']; 
            }
            $applicantsNumber = Applicant::where('post_id', $value->id)->get()->count();
            $returnPosts[$key]['number_of_applicants'] = $applicantsNumber; 
            $returnObj[] = $returnPosts[$key];
        }
        return response()->json([
            'posts' => $returnObj
        ],200);
    }
    public function getFavorites(Request $request)
    {
        $user = $request->user();
        $favoriteIDs = Favorite::where('user_id', $user->id)->get('post_id');
        foreach ($favoriteIDs as $key => $value) {
            $favoriteIDs[$key] = $value->post_id;
        }
        $favorites = [];
        foreach ($favoriteIDs as $id) {
            $post = Post::find($id);
            if (!$post) {
                Favorite::where('user_id', $user->id)->where('post_id', $id)->delete();
                continue;
            }
            if ($post->deleted) {
                Favorite::where('user_id', $user->id)->where('post_id', $id)->delete();
                continue;
            }
            if (!$post->accepted) {
                continue;
            }
            $arr = TagController::getPostTagsAndCategories($post->id);
            $post['tags'] = $arr['tags'];
            $post['categories'] = $arr['categories'];
            $ratings = Post_rating::where('post_id', $post->id)->get();
            $post['ratings'] = $ratings;
            if ($post->image) {
                // $image = public_path('/images/posts/') . $post->image;
                $post['image'] = URL::to('/') . "/images/posts/$post->image";
            }
            $city = City::find($post->location);
            if ($city) {
                $post['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $post['country'] = $country['name'];
            }
            $postType = Post_type::find($post->type_id);
            $post['post_type'] = $postType['type'];
            $applicantsNumber = Applicant::where('post_id', $post->id)->get()->count();
            $post['number_of_applicants'] = $applicantsNumber;
            $post['total_rating'] = self::getPostTotalRating($post->id);
            $owner = User::find($post->user_id);
            $userArr = [];
            $userType = Account_type::find($owner->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $owner->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $owner->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($owner->image) {
                $userArr['image'] = URL::to('/') . "/images/profile/$owner->image";
            }
            $post['user'] = $userArr;
            $favorites[] = $post;
        }

        return response()->json([
            'favorites' => $favorites
        ]);
    }
    public function getUserPosts(Request $request)
    {
        $request->validate([
            'user_id' => ['exists:users,id'],
        ]);
        $returnPosts = Post::where('user_id', $request->user_id)->get(); 
        foreach ($returnPosts as $key => $value) {
            $arr = TagController::getPostTagsAndCategories($value->id);
            $returnPosts[$key]['tags'] = $arr['tags'];
            $returnPosts[$key]['categories'] = $arr['categories'];
            $ratings = Post_rating::where('post_id', $value->id)->get();
            $returnPosts[$key]['ratings'] = $ratings;
            if ($value->image) {
                // $image = public_path('/images/posts/') . $value->image;
                $returnPosts[$key]['image'] = URL::to('/') . "/images/posts/$value->image";
            }
            $city = City::find($value->location);
            if ($city) {
                $returnPosts[$key]['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $returnPosts[$key]['country'] = $country['name'];
            }
            $postType = Post_type::find($value->type_id);
            $returnPosts[$key]['post_type'] = $postType['type'];
            $applicantsNumber = Applicant::where('post_id', $value->id)->get()->count();
            $returnPosts[$key]['number_of_applicants'] = $applicantsNumber;
            $returnPosts[$key]['total_rating'] = self::getPostTotalRating($value->id);
        }
        return response()->json([
            'posts' => $returnPosts
        ],200);
    }
    public function createPost(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'type' => 'required|exists:post_types,type',
            'title' => 'required',
            'about' => 'required',
            'money' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,svg',
            'tags' => 'array',
            'categories' => 'array'
        ]);
        $user = $request->user();
        $user_package = User_package::where('user_id', $user->id)->where('still_available', true)->first();
        if (!$user_package) {
            return response()->json([
                'message' => 'buy a new package then you can make the post',
            ]);
        }
        $package = Package::find($user_package->package_id);
        if($package->availble_for == 'day'){
            $expired_date = $user_package->created_at->addDay();
        }else if($package->availble_for == 'month'){
            $expired_date = $user_package->created_at->addMonth();
        }else if($package->availble_for == 'year'){
            $expired_date = $user_package->created_at->addYear();
        }
        if (!now()->lessThanOrEqualTo($expired_date)) {
            $user_package->still_available = false;
            $user_package->save();
            return response()->json([
                'message' => 'your package is out of date, please buy a new one'
            ]);
        }
        if ($user_package->still_have_posts == 1) {
            $user_package->still_have_posts = 0;
            $user_package->still_available = false;
        }else {
            $user_package->still_have_posts = $user_package->still_have_posts - 1;
        }
        $temp = $request->all();
        $temp['user_id'] = $user['id'];
        $postTypeObg = Post_type::where('type', $temp['type'])->first();
        $temp['type_id'] = $postTypeObg['id'];

        if ($temp['type'] == 'job') {
            $request->validate([
                'employment_type' => [
                    Rule::in([
                        'Full time',
                        'Part time',
                        'Contract',
                        'Remotely'
                        ]),
                ],
                'location' => 'required|exists:cities,city',
            ]);
            if (!$user->tokenCan('company')){
                return response()->json([
                    'message' => 'this user cannot post this type'
                ],403);
            }
        }
        if ($request->has('location')) {
            $city = City::where('city', $temp['location'])->first();
            $temp['location'] = $city['id'];   
        }
        if ($request->has('image')){
            $imageName = time().'.'.$request->file('image')->extension(); 
            $request->file('image')->move(public_path('images/posts'), $imageName);
            $temp['image'] = $imageName;
        }
        $newPost = Post::create($temp);
        $user_package->save();
        
        $tags = $temp['tags'];
        foreach ($tags as $key => $value) {
            TagController::addTagOrCategoryToPost($newPost, $value, 'tag');
        }
        $categories = $temp['categories'];
        foreach ($categories as $key => $value) {
            TagController::addTagOrCategoryToPost($newPost, $value, 'category');
        }
        $arr = TagController::getPostTagsAndCategories($newPost->id);
        $newPost['tags'] = $arr['tags'];
        $newPost['categories'] = $arr['categories'];
        if ($newPost->image) {
            // $image = public_path('/images/posts/') . $newPost->image;
            $newPost['image'] = URL::to('/') . "/images/posts/$newPost->image";
        }
        if ($newPost->location) {
            $city = City::find($newPost->location);
            if ($city) {
                $newPost['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $newPost['country'] = $country['name'];
            }
        }
        $postType = Post_type::find($newPost->type_id);
        $newPost['post_type'] = $postType['type'];
        return response()->json([
            'post' => $newPost
        ],200);
    }
    public function updatePostById(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'id' => 'required',
            'new_tags' => 'array',
            'new_categories' => 'array',
            'employment_type' => [
                Rule::in([
                    'Full time',
                    'Part time',
                    'Contract',
                    'Remotely'
                    ]),
            ],
            'new_image' => 'image|mimes:jpeg,png,jpg,svg',
            'new_location' => 'exists:cities,city',
        ]);
        $user = $request->user();
        $post = Post::find($request->input('id'));
        if (!$this->ownerOfThePost($user, $post)){
            return response()->json([
                'message' => 'this user cant edit this post'
            ], 403);
        }
        if ($post['modified'] == true) {
            return response()->json([
                'message' => 'you cannot update the post more than once',
            ], 406);
        }
        if($request->hasAny(['user_id', 'type_id'])){
            return response()->json([
                'message' => 'you cannot change user or type',
            ], 406);
        }
        $input = $request->all();
        $input['modified'] = true;
        $input['accepted'] = false;
        if ($request->has('new_location')) {
            $city = City::where('city', $request['new_location'])->first();
            $input['location'] = $city['id'];
        }
        if ($request->has('new_image')) {
            if ($post['image']) {
                if(File::exists(public_path('/images/posts/') . $post['image'])){
                    unlink(public_path('/images/posts/') . $post['image']);
                }
            }
            $imageName = time().'.'.$request->file('new_image')->extension(); 
            $request->file('new_image')->move(public_path('images/posts'), $imageName);
            $input['image'] = $imageName;
        }
        $post->fill($input)->save();

        if ($request->has('new_tags')){
            TagController::deleteTagOrCategoryOfPost($post, 'tag', true);
            $tags = $request->input('new_tags');
            foreach ($tags as $key => $value) {
                TagController::addTagOrCategoryToPost($post, $value, 'tag');
            }
        }
        if ($request->has('new_categories')){
            TagController::deleteTagOrCategoryOfPost($post, 'category', true);
            $categories = $request->input('new_categories');
            foreach ($categories as $key => $value) {
                TagController::addTagOrCategoryToPost($post, $value, 'category');
            }
        }
        $arr = TagController::getPostTagsAndCategories($post->id);
        $post['tags'] = $arr['tags'];
        $post['categories'] = $arr['categories'];
        $ratings = Post_rating::where('post_id', $post->id)->get();
        $post['ratings'] = $ratings;
        if ($post->image) {
            // $image = public_path('/images/posts/') . $value->image;
            $post['image'] = URL::to('/') . "/images/posts/$value->image";
        }
        $city = City::find($post->location);
        $post['location'] = $city['city'];
        $country = Country::where('code', $city->iso2)->first();
        $post['country'] = $country['name'];
        $postType = Post_type::find($post->type_id);
        $post['post_type'] = $postType['type'];
        return response()->json([
            'post' => $post,
        ], 200);
    }
    public function deletePostById(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'id' => 'required',
        ]);
        $user = $request->user();
        $post = Post::find($request->input('id'));
        if (!$this->ownerOfThePost($user, $post) && !$user->tokenCan('admin')){
            return response()->json([
                'message' => 'this user cant delete this post'
            ], 403);
        }
        $post['deleted'] = true;
        $post->save();
        return response()->json([
            'message' => 'done'
        ],);
    }
    public function stopPostById(Request $request)
    {
        $request->validate([
            'post_id' => 'required',
        ]);
        $user = $request->user();
        $post = Post::find($request->input('post_id'));
        if (!$this->ownerOfThePost($user, $post)){
            return response()->json([
                'message' => 'this user cant stop this post'
            ], 403);
        }
        $post['is_waiting'] = false;
        $post->save();
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function reportPostById(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
            'reason' => ['required'],
        ]);
        $user = $request->user();
        Report::create([
            'user_id' => $user->id,
            'post_id' => $request->post_id,
            'reason' => $request->reason
        ]);
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function ratePostById(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
            'rating' => ['required', Rule::in(['1', '2', '3', '4', '5'])],
            'rating_text' => ['required'],
        ]);
        $user = $request->user();
        $post = Post::find($request->post_id);
        if($this->ownerOfThePost($user, $post)) {
            return response()->json([
                'message' => 'you are the owner of this post,  you cannot rate it'
            ]);
        }
        $postType = Post_type::find($post->type_id);
        if ($postType->type == 'service') {
            $applicant = Applicant::where('user_id', $user->id)
                ->where('post_id', $post->id)
                ->where('application_status', 'accepted')
                ->first();
            if (!$applicant) {
                return response()->json([
                    'message' => 'you cannt rate this  till you try it'
                ]);
            }
        }
        $rate = Post_rating::where('post_id', $request->post_id)->where('user_id', $user->id)->first();
        if ($rate) {
            return response()->json([
                'message' => 'you already rated this post'
            ]);
        }
        Post_rating::create([
            'user_id' => $user->id,
            'post_id' => $request->post_id,
            'rating' => $request->rating,
            'rating_text' => $request->rating_text
        ]);
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function getApplicants(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
        ]);
        $user = $request->user();
        $post = Post::find($request->post_id);
        if (!$this->ownerOfThePost($user, $post)) {
            return response()->json([
                'message' => 'you are not the owner of this post'
            ]);
        }
        $applicants = Applicant::where('post_id', $post->id)->get();
        foreach ($applicants as $key => $value) {
            $myUser = User::find($value->user_id);
            $my_details = User_details::where('user_id', $myUser->id)->first();
            if ($myUser->image) {
                $applicants[$key]['image'] = URL::to('/') . "/images/profile/$myUser->image";
            }
            $applicants[$key]['name'] = $my_details->first_name . ' ' . $my_details->last_name;
        }
        return response()->json([
            'applicants' => $applicants
        ]);
    }
    public function acceptApplicant(Request $request)
    {
        $request->validate([
            'applicant_id' => ['required', 'exists:applicants,id'],
        ]);
        $user = $request->user();
        $applicant = Applicant::find($request->applicant_id);
        $post = Post::find($applicant->post_id);
        if (!$this->ownerOfThePost($user, $post)) {
            return response()->json([
                'message' => 'you are not the owner of this post'
            ]);
        }
        if ($applicant->application_status != 'waiting') {
            return response()->json([
                'message' => 'you cant change the status of the applicant'
            ]);
        }
        $applicant->application_status = 'accepted';
        $applicant->seen_by_user = false;
        $applicant->save();
        //TODO: send notification to the accepted user
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function refuseApplicant(Request $request)
    {
        $request->validate([
            'applicant_id' => ['required', 'exists:applicants,id'],
        ]);
        $user = $request->user();
        $applicant = Applicant::find($request->applicant_id);
        $post = Post::find($applicant->post_id);
        if (!$this->ownerOfThePost($user, $post)) {
            return response()->json([
                'message' => 'you are not the owner of this post'
            ]);
        }
        if ($applicant->application_status != 'waiting') {
            return response()->json([
                'message' => 'you cant change the status of the applicant'
            ]);
        }
        $applicant->application_status = 'refused';
        $applicant->seen_by_user = false;
        $applicant->save();
        //TODO: send notification to the refused user
        return response()->json([
            'message' => 'done'
        ]);
    }

    public static function getPostTotalRating($post_id)
    {
        $ratings = Post_rating::where('post_id', $post_id)->get();
        if (count($ratings) == 0) {
            return 0;
        }
        $sum = 0;
        foreach ($ratings as $rating) {
            $sum += $rating->rating;
        }
        return $sum/count($ratings);
    }
    // checks if a user is the owner of a post...
    private function ownerOfThePost(User $user, Post $post): bool
    {
        return $user['id'] == $post['user_id'];
    }
}
