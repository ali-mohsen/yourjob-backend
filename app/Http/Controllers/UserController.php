<?php

namespace App\Http\Controllers;

use App\Models\Account_type;
use App\Models\Applicant;
use App\Models\City;
use App\Models\Company_details;
use App\Models\Country;
use App\Models\Education_level;
use App\Models\Favorite;
use App\Models\Institute_type;
use App\Models\Language;
use App\Models\Message;
use App\Models\Number_of_employees;
use App\Models\Package;
use App\Models\Post;
use App\Models\Post_rating;
use App\Models\Post_tag;
use App\Models\Review;
use App\Models\Tag;
use App\Models\User;
use App\Models\User_course;
use App\Models\User_details;
use App\Models\User_language;
use App\Models\User_package;
use App\Models\User_tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class UserController extends Controller
{
    //this password is for creating admin accounts
    protected $dashboard_password = '123';

    public function getAllUsers(): \Illuminate\Http\JsonResponse
    {
        $allUsers = User::where('deleted', false)->get();
        $users = [];
        $companies = [];
        $admins = [];
        foreach ($allUsers as $key => $value) {
            $accountType = Account_type::find($value->account_type_id);
            if ($accountType->type == 'normal_user') {
                $details = User_details::where('user_id', $value->id)->first();
                $allUsers[$key]['user_type'] = 'normal_user';
            }elseif ($accountType->type == 'company') {
                $details = Company_details::where('user_id', $value->id)->first();
                $instituteType = Institute_type::find($details->institute_type_id);
                $details->institute_type = $instituteType->type;
                $numberOfEmployees = Number_of_employees::find($details->number_of_employees);
                $details->number_of_employees = $numberOfEmployees->num_of_emp;
                $allUsers[$key]['user_type'] = 'company';
            }else{ //admin
                $details = null;
                $allUsers[$key]['user_type'] = 'admin';
                $admins[] = $allUsers[$key];
                continue;
            }
            $allUsers[$key]['details'] = $details;
            $arr = TagController::getUserTagsAndCategories($value->id);
            $allUsers[$key]['tags'] = $arr['tags'];
            $allUsers[$key]['categories'] = $arr['categories'];
            $reviews = Review::where('user_2_id', $value->id)->get(['user_1_id as reviewer_id', 'review']);
            $allUsers[$key]['reviews'] = $reviews;
            if ($value->image) {
                $allUsers[$key]['image'] = URL::to('/') . "/images/profile/$value->image";
            }
            if ($value->city_id) {
                $city = City::find($value->city_id);
                $allUsers[$key]['city'] = $city['city'];
            }
            if ($value->country_id) {
                $country = Country::find($value->country_id);
                $allUsers[$key]['country'] = $country['name'];
            }
            $userPosts = Post::where('user_id', $value->id)->where('deleted', false)->get();
            $allUsers[$key]['number_of_posts'] = $userPosts->count();

            if ($accountType->type == 'normal_user') {
                $users[] =  $allUsers[$key];
            }elseif ($accountType->type == 'company') {
                $companies[] =  $allUsers[$key];
            }
        }
        return response()->json([
            'users' => $users,
            'companies' => $companies,
            'admins' => $admins
        ], 200);
    }
    public function getUserById(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'user_id' => ['required', 'exists:users,id'],
        ]);
        $user = User::find($request->user_id);
        if($user['deleted']){
            return response()->json([
                'message' => 'this user has been deleted'
            ], 406);
        }
        $accountType = Account_type::find($user->account_type_id);
        if ($accountType->type == 'normal_user') {
            $details = User_details::where('user_id', $user->id)->first();
            $user['user_type'] = 'normal_user';
            $languages = User_language::where('user_id', $user['id'])->get();
            foreach ($languages as $languageKey => $language) {
                $lang = Language::find($language->language_id);
                $languages[$languageKey] = $lang->name;
            }
            $details['languages'] = $languages;
            $courses = User_course::where('user_id', $user['id'])->first();
            $details['courses'] = $courses->course_title;
            $educationLevel = Education_level::find($details->education_level_id);
            $details['education_level'] = $educationLevel->education_level;
        }elseif ($accountType->type == 'company') {
            $details = Company_details::where('user_id', $user->id)->first();
            $instituteType = Institute_type::find($details->institute_type_id);
            $details->institute_type = $instituteType->type;
            $numberOfEmployees = Number_of_employees::find($details->number_of_employees);
            $details->number_of_employees = $numberOfEmployees->num_of_emp;
            if ($details->verfication) {
                $details->verfication = URL::to('/') . "/images/verification/$details->verfication";
            }
            $user['user_type'] = 'company';
        }else{ //admin
            $details = null;
            $user['user_type'] = 'admin';
        }
        $user['details'] = $details;
        $arr = TagController::getUserTagsAndCategories($user->id);
        $user['tags'] = $arr['tags'];
        $user['categories'] = $arr['categories'];
        $reviews = Review::where('user_2_id', $user->id)->get(['user_1_id as reviewer_id', 'review']);
        $user['reviews'] = $reviews;
        if ($user->image) {
            // $image = public_path('/images/profile/') . $user->image;
            $user['image'] = URL::to('/') . "/images/profile/$user->image";
        }
        $city = City::find($user->city_id);
        if ($city) {
            $user['city'] = $city['city'];
            $country = Country::find($user->country_id);
            $user['country'] = $country['name'];
        }
        $userPosts = Post::where('user_id', $user->id)->where('deleted', false)->get();
        $user['number_of_posts'] = $userPosts->count();
        return response()->json([
            'user' => $user
        ], 200);
    }
    public function registerAdmin(Request $request){
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'dashboard_password' =>'required',
        ]);

        if($request->input('dashboard_password') != $this->dashboard_password){
            return response()->json([
                'message' => 'the dashboared password is not correct'
            ]);
        }

        $temp = $request->all();
        $newAccountType = Account_type::where('type', 'admin')->first();
        $temp['account_type_id'] = $newAccountType['id'];
        $temp['password'] = Hash::make($temp['password']);
        $newUser = User::create($temp);
        return response()->json([
            'created user' => $newUser,
        ], 201);

    }
    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'account_type' => 'required|exists:account_types,type',
            'phone_number' => 'required|unique:users|numeric',
            'country' => 'required',
            'city' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,svg',

            'tags' => 'array',
            'categories' => 'array'
        ]);
        $temp = $request->all();
        $type = $temp['account_type'];
        if($type == 'admin'){
            return response()->json([
                'message' => 'not authorised to create an admin hear !!!'
            ], 403);
        }
        elseif($type == 'normal_user'){
            $request->validate([
                'education_level' => 'required',
                'languages' => 'array',
                'courses' => '',
                'first_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required'
            ]);
        }
        else{
            $request->validate([
                'verification' => 'required|image|mimes:jpeg,png,jpg,svg',
                'institute_type' => 'required',
                'institute_name' => 'required',
                'number_of_employees' => 'required|exists:number_of_employees,num_of_emp',
                'institute_field' => 'required',
            ]);
        }
        if ($request->has('image')) {
            $imageName = $temp['email'].time().'.'.$request->file('image')->extension(); 
            $request->file('image')->move(public_path('images/profile'), $imageName);
            $temp['image'] = $imageName;
        }

        $accountType = Account_type::where('type', $type)->first();
        $temp['account_type_id'] = $accountType['id'];
        $temp['password'] = Hash::make($temp['password']);
        $country = Country::where('name', $temp['country'])->first();
        $temp['country_id'] = $country['id'];
        $city = City::where('city', $temp['city'])->first();
        $temp['city_id'] = $city['id'];

        $newUser = User::create($temp);

        $tags = $temp['tags'];
        foreach ($tags as $key => $value) {
            TagController::addTagOrCategoryToUser($newUser, $value, 'tag');
        }
        $categories = $temp['categories'];
        foreach ($categories as $key => $value) {
            TagController::addTagOrCategoryToUser($newUser, $value, 'category');
        }

        $details = null;
        switch ($type) {
            case 'normal_user':
                $educationLevel = Education_level::where('education_level', $temp['education_level'])->first();
                $temp['education_level_id'] = $educationLevel['id'];

                $languages = $temp['languages'];
                foreach ($languages as $language) {
                    $language = Language::where('name', $language)->first();
                    $tempUserLanguage = [];
                    $tempUserLanguage['user_id'] = $newUser['id'];
                    $tempUserLanguage['language_id'] = $language['id'];
                    User_language::create($tempUserLanguage);
                }

                // $courses = $temp['courses'];
                // foreach ($courses as $course) {
                //     $course['user_id'] = $newUser['id'];
                //     User_course::create($course);
                // }
                $courses['course_title'] = $temp['courses'];
                $courses['user_id'] = $newUser['id'];
                User_course::create($courses);
                $temp['user_id'] = $newUser['id'];
                $details = User_details::create($temp);
                break;

            case 'company':
                $instituteType = Institute_type::where('type', $temp['institute_type'])->first();
                $temp['institute_type_id'] = $instituteType['id'];
                $numOfEmp = Number_of_employees::where('num_of_emp', $temp['number_of_employees'])->first();
                if ($numOfEmp) {
                    $temp['number_of_employees'] = $numOfEmp['id'];
                }
                //verification image
                $verificationImageName = $temp['email'].'.'.$request->file('verification')->extension();  
                $request->file('verification')->move(public_path('images/verification'), $verificationImageName);
                $temp['verfication'] = $verificationImageName;
                $temp['user_id'] = $newUser['id'];
                $details = Company_details::create($temp);
                $instituteType = Institute_type::find($details->institute_type_id);
                $details->institute_type = $instituteType->type;
                $numberOfEmployees = Number_of_employees::find($details->number_of_employees);
                $details->number_of_employees = $numberOfEmployees->num_of_emp;
                if ($details->verfication) {
                    $details['verfication'] = URL::to('/') . "/images/verfication/$details->verfication";
                }
                break;

            default:
                return response()->json([
                    'message' => 'something went wrong ask Ali about it now.',
                ], 403);
                break;
        }
        $arr = TagController::getUserTagsAndCategories($newUser->id);
        $newUser['tags'] = $arr['tags'];
        $newUser['categories'] = $arr['categories'];
        $reviews = Review::where('user_2_id', $newUser->id)->get(['user_1_id as reviewer_id', 'review']);
        $newUser['reviews'] = $reviews;
        if ($newUser->image) {
            // $image = public_path('/images/profile/') . $newUser->image;
            $newUser['image'] = URL::to('/') . "/images/profile/$newUser->image";
        }
        $city = City::find($newUser->city_id);
        $newUser['city'] = $city['city'];
        $country = Country::find($newUser->country_id);
        $newUser['country'] = $country['name'];
        $newUser['details'] = $details;
        return response()->json([
            'created user' => $newUser
        ], 201);
    }
    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $user = User::where('email', $request->input('email'))->first();
        if (!$user || !$this->checkPassword($user, $request->input('password'))){
            //if (!$user || $request->password != $user['password']){
            return response()->json([
                'message' => 'email and password do not  match',
            ]);
        }
        $user->tokens()->delete();
        if($user['deleted']){
            return response()->json([
                'message' => 'this user has been deleted'
            ]);
        }

        $accountType = Account_type::find($user['account_type_id']);
        $arr = TagController::getUserTagsAndCategories($user->id);
        $user['tags'] = $arr['tags'];
        $user['categories'] = $arr['categories'];
        $reviews = Review::where('user_2_id', $user->id)->get(['user_1_id as reviewer_id', 'review']);
        $user['reviews'] = $reviews;
        if ($user->image) {
            // $image = public_path('/images/profile/') . $user->image;            
            $user['image'] = URL::to('/') . "/images/profile/$user->image";
        }
        if ($user->city_id) {
            $city = City::find($user->city_id);
            $user['city'] = $city['city'];
        }
        if ($user->country_id) {
            $country = Country::find($user->country_id);
            $user['country'] = $country['name'];
        }
        $accountType = Account_type::find($user->account_type_id);
        if ($accountType->type == 'normal_user') {
            $details = User_details::where('user_id', $user->id)->first();
            $languages = User_language::where('user_id', $user->id)->get();
            foreach ($languages as $languageKey => $language) {
                $lang = Language::find($language->language_id);
                $languages[$languageKey] = $lang->name;
            }
            $details['languages'] = $languages;
        }elseif ($accountType->type == 'company') {
            $details = Company_details::where('user_id', $user->id)->first();
            if ($details->verified == false) {
                return response()->json([
                    'message' => 'this company is not verified, you cant login'
                ]);
            }
            $instituteType = Institute_type::find($details->institute_type_id);
            $details->institute_type = $instituteType->type;
            $numberOfEmployees = Number_of_employees::find($details->number_of_employees);
            $details->number_of_employees = $numberOfEmployees->num_of_emp;
        }else{ //admin
            $details = null;
        }
        $userPosts = Post::where('user_id', $user->id)->where('deleted', false)->get();
        $user['number_of_posts'] = $userPosts->count();

        $packages = User_package::where('user_id', $user->id)->where('still_available', true)->get('still_have_posts');
        $user['still_have_posts'] = 0;
        foreach ($packages as $package) {
            $user['still_have_posts'] += $package->still_have_posts;
        }
        $user['details'] = $details;
        $token = $user->createToken('your-job-token', [$accountType['type']])->plainTextToken;
        return response()->json([
            'user' => $user,
            'token' => $token,
            'message' => 'done'
        ], 200);
    }
    public function updateUser(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'password' => 'required',
            'new_image' => 'image|mimes:jpeg,png,jpg,svg',
        ]);
        $user =  $request->user();
        if (!$this->checkPassword($user, $request->input('password'))){
            return response()->json([
                'message' => 'wrong password',
            ]);
        }
        if($request->hasAny(['email', 'account_type_id'])){
            return response()->json([
                'message' => 'you cannot change email or account type',
            ], 406);
        }
        if ($request->has('new_password')){
            $user['password'] = Hash::make($request['new_password']);
        }
        if ($request->has('new_image')){
            if ($user['image']) {
                if(File::exists(public_path('/images/profile/') . $user['image'])){
                    unlink(public_path('/images/profile/') . $user['image']);
                }
            }
            $imageName = $user['email'].time().'.'.$request->file('new_image')->extension(); 
            $request->file('new_image')->move(public_path('images/profile'), $imageName);
            $user['image'] = $imageName;
        }
        if ($request->has('new_phone_number')){
            $user['phone_number'] = $request['new_phone_number'];
        }
        if ($request->has('new_country')){
            $country = Country::where('name', $request['new_country'])->first();
            $user['country_id'] = $country['id'];
        }
        if ($request->has('new_city')){
            $city = city::where('city', $request['new_city'])->first();
            $user['city_id'] = $city['id'];
        }
        $user->save();
        
        $accountType = Account_type::find($user['account_type_id']);

        if ($accountType['type'] == 'normal_user'){
            $details = User_details::where('user_id', $user['id'])->first();
            if ($request->has('new_birth_year')){
                $details['birth_year'] = $request->input('new_birth_year');
            }
            if ($request->has('new_first_name')){
                $details['first_name'] = $request->input('new_first_name');
            }
            if ($request->has('new_last_name')){
                $details['last_name'] = $request->input('new_last_name');
            }
            if ($request->has('new_gender')){
                $details['gender'] = $request->input('new_gender');
            }
            if ($request->has('new_main_profession')){
                $details['main_profession'] = $request->input('new_main_profession');
            }
            if ($request->has('new_work_experience')){
                $details['work_experience'] = $request->input('new_work_experience');
            }
            if ($request->has('new_education')){
                $details['education'] = $request->input('new_education');
            }
            if ($request->has('new_skills')){
                $details['skills'] = $request->input('new_skills');
            }
            if ($request->has('new_education_level')){
                $newEducationLevel = Education_level::where('education_level', $request['new_education_level'])->first();
                $details['education_level_id'] = $newEducationLevel['id'];
            }
            $details->save();
        }
        elseif ($accountType['type'] == 'company'){
            $details = Company_details::where('user_id', $user['id']);
            //you can't change now institute_type_id
            if($request->hasAny(['new_institute_type'])){
                return response()->json([
                    'message' => 'you cannot change institute_types',
                ], 406);
            }
            if ($request->has('new_institute_name')){
                $details['institute_name'] = $request->input('new_institute_name');
            }
            if ($request->has('new_institute_field')){
                $details['institute_field'] = $request->input('new_institute_field');
            }
            if ($request->has('new_about')){
                $details['about'] = $request->input('new_about');
            }
            if ($request->has('new_number_of_employees')){
                $numOfEmp = Number_of_employees::where('num_of_emp', $request->input('new_number_of_employees'))->first();
                $details['number_of_employees'] = $numOfEmp['id'];
            }
            $details->save();
        }
        else
            $details = 'this is an admin account';
        $arr = TagController::getUserTagsAndCategories($user->id);
        $user['tags'] = $arr['tags'];
        $user['categories'] = $arr['categories'];
        $reviews = Review::where('user_2_id', $user->id)->get(['user_1_id as reviewer_id', 'review']);
        $user['reviews'] = $reviews;
        if ($user->image) {
            // $image = public_path('/images/profile/') . $user->image;
            $user['image'] = URL::to('/') . "/images/profile/$user->image";
        }
        $city = City::find($user->city_id);
        if ($city) {
            $user['city'] = $city['city'];
            $country = Country::find($user->country_id);
            $user['country'] = $country['name'];
        }
        $userPosts = Post::where('user_id', $user->id)->where('deleted', false)->get();
        $user['number_of_posts'] = $userPosts->count();
        $user['details'] = $details;
        return response()->json([
            'user' => $user
        ], 200);
    }
    public function deleteUser(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'password' => 'required',
        ]);
        $user = $request->user();
        if (!$user || !$this->checkPassword($user, $request->input('password'))){
            return response()->json([
                'message' => 'wrong password',
            ], 401);
        }
        $user->tokens()->delete();
        $user['deleted'] = true;
        $user->save();
        return response()->json([
            'message' => 'deleted'
        ], 200);
    }
    public function deleteUserById(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'id' => 'required',
        ]);
        $user = $request->user();
        if (!$user->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission to delete other accounts'
            ], 403);
        }
        $userToDelete = User::find($request->input('id'));
        if ($userToDelete == null){
            return response()->json([
                'message' => 'this account is not found'
            ], 404);
        }
        $userToDelete->tokens()->delete();
        $userToDelete['deleted'] = true;
        $userToDelete->save();
        return response()->json([
            'message' => 'user deleted'
        ], 200);
    }
    public function addLanguage(Request $request){
        $request->validate([
            'new_language' => 'required',
        ]);
        $user = $request->user();
        if(!$this->checkNormalUser($user)){
            return response()->json([
                'message' => 'this user cannot add a language'
            ], 406);
        }
        $language = $request['new_language'];
        $language = Language::where('name', $language)->first();
        $tempUserLanguage = [];
        $tempUserLanguage['user_id'] = $user['id'];
        $tempUserLanguage['language_id'] = $language['id'];
        User_language::firstOrCreate($tempUserLanguage);
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public function addCourse(Request $request){
        $request->validate([
            'course_title' => 'required',
            //'about_the_course' => 'required',
        ]);
        $user = $request->user();
        if(!$this->checkNormalUser($user)){
            return response()->json([
                'message' => 'this user cannot add a course'
            ], 406);
        }
        $Newcourse = [];
        $Newcourse['user_id'] = $user['id'];
        $Newcourse['course_title'] = $request['course_title'];
        if ($request->has('about_the_course')) {
            $Newcourse['about_the_course'] = $request['about_the_course'];
        }
        User_course::updateOrCreate([
            'user_id' => $user['id'],
            'course_title' => $request['course_title']
        ], $Newcourse);
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public function deleteLanguage(Request $request)
    {
        $request->validate([
            'language' => 'required',
        ]);
        $user = $request->user();
        $language = $request['language'];
        $language = Language::where('name', $language)->first();
        DB::table('user_languages')->where([
            ['user_id', '=', $user['id']],
            ['language_id', '=', $language['id']],
        ])->delete();
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public function deleteCourse(Request $request)
    {
        $request->validate([
            'course_title' => 'required',
        ]);
        $user = $request->user();
        DB::table('user_courses')->where([
            ['user_id', '=', $user['id']],
            ['course_title', '=', $request->input('course_title')],
        ])->delete();
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public function updateUserTags(Request $request)
    {
        $request->validate([
            'new_tags' => 'required|array'
        ]);
        $user = $request->user();
        TagController::deleteTagOrCategoryOfUser($user, 'tag', true);

        $tags = $request->input('new_tags');
        foreach ($tags as $key => $value) {
            TagController::addTagOrCategoryToUser($user, $value, 'tag');
        }
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public function reviewUserById(Request $request)
    {
        $request->validate([
            'user_2_id' => 'required|exists:users,id',
            'review' => 'required'
        ]);

        $user = $request->user();
        if($request->input('user_2_id') == $user['id']){
            return response()->json([
                'message' => 'you cannot review yourself'
            ], 406);
        }
        Review::create([
            'user_1_id' => $user['id'],
            'user_2_id' => $request->input('user_2_id'),
            'review' => $request->input('review')
        ]);
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public function updateUserCategories(Request $request)
    {
        $request->validate([
            'new_categories' => 'required|array'
        ]);
        $user = $request->user();
        TagController::deleteTagOrCategoryOfUser($user, 'category', true);

        $categories = $request->input('new_categories');
        foreach ($categories as $key => $value) {
            TagController::addTagOrCategoryToUser($user, $value, 'category');
        }
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public function getAllPackages(){
        return Package::all();
    }
    public function buyPackage(Request $request)
    {
        $request->validate([
            'package_id' => ['required', 'exists:packages,id'],
        ]);

        $user = $request->user();
        $package = Package::find($request->package_id);
        if ($user->account_money < $package->price) {
            return response()->json([
                'message' => 'you do not have enough money'
            ]);
        }

        User_package::create([
            'user_id' => $user->id,
            'package_id' => $request->package_id,
            'still_have_posts' =>  $package->posts_number,
        ]);

        $user->account_money -= $package->price;
        $user->save();
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function apply(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
        ]);
        $user = $request->user();
        $userType = Account_type::find($user->account_type_id);
        if ($userType->type != 'normal_user') {
            return response()->json([
                'message' => 'only normal users can apply to this...'
            ]);
        }
        $exists =  Applicant::where('user_id',  $user->id)->where('post_id',   $request->post_id)->first();
        if ($exists) {
            return response()->json([
                'message' => 'you already applied to this...'
            ]);
        }else {
            Applicant::create([
                'user_id' => $user->id,
                'post_id' => $request->post_id,
                'application_status' => 'waiting'
            ]);
        }

        return response()->json([
            'message' => 'done'
        ]);
    }
    public function sendMessage(Request $request)
    {
        $request->validate([
            'receiver_id' => ['exists:users,id'],
            'post_id' => ['required', 'exists:posts,id'],
            'message' => ['required'],
        ]);
        $user = $request->user();
        $newMessage = [];
        $newMessage['sender_id'] = $user->id;
        if ($request->has('receiver_id')) {
            $newMessage['receiver_id'] = $request->receiver_id;
        }else {
            $post = Post::find($request->post_id);
            $newMessage['receiver_id'] = $post->user_id;
        }
        $newMessage['post_id'] = $request->post_id;
        $newMessage['message'] = $request->message;
        Message::create($newMessage);
        //TODO: send notification to the receiver
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function receiveAllMessages(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'] 
        ]);
        $user = $request->user();
        $messages = Message::where('post_id', $request->post_id)
        ->where(function ($query) use ($user) {
            $query->where('sender_id', $user->id)
                  ->orWhere('receiver_id',  $user->id);
        })->get();
        return response()->json([
            'messages' => $messages
        ]);
    }
    public function findEmail(Request $request)
    {
        $request->validate([
            'email' => 'required'
        ]);
        $newUser = User::where('email', $request->email)->first();
        if ($newUser) {
            $emailFound = true;
        }
        else {
            $emailFound = false;
        }
        return response()->json([
            'already_have_account' => $emailFound
        ]);
    }
    public function addPostToFavorite(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
        ]);
        $user = $request->user();
        $exists = Favorite::where('user_id', $user->id)->where('post_id', $request->post_id)->first();
        if (!$exists) {
            Favorite::create([
                'user_id' => $user->id,
                'post_id' => $request->post_id,
            ]);
            return response()->json([
                'message' => 'done'
            ]);
        }
        return response()->json([
            'message' => 'already in favorite'
        ]);
    }
    public function deletePostFromFavorite(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
        ]);
        $user = $request->user();
        $exists = Favorite::where('user_id', $user->id)->where('post_id', $request->post_id)->first();
        if ($exists) {
            Favorite::where('user_id', $user->id)->where('post_id', $request->post_id)->delete();
        }else{
            return response()->json([
                'message' => 'this post is not in the favorite'
            ], 404);
        }
        return response()->json([
            'message' => 'done'
        ]);
    }
    public function notifications(Request $request)
    {
        $user = $request->user();
        $yourRequests = Applicant::where('user_id', $user->id)->where('seen_by_user', false)->get();
        Applicant::where('user_id', $user->id)->where('seen_by_user', false)->update([
            'seen_by_user' => true
        ]);
        foreach ($yourRequests as $key => $value) {
            $myPost = Post::find($value->post_id);
            if ($myPost->image) {
                $myPost['image'] = URL::to('/') . "/images/posts/$value->image";
            }
            if ($myPost->location) {
                $city = City::find($myPost->location);
                if ($city) {
                    $myPost['location'] = $city['city'];
                    $country = Country::where('code', $city->iso2)->first();
                    $myPost['country'] = $country['name']; 
                }
            }
            $applicantsNumber = Applicant::where('post_id', $myPost->id)->get()->count();
            $myPost['number_of_applicants'] = $applicantsNumber; 
            $owner = User::find($myPost->user_id);
            $userArr = [];
            $userType = Account_type::find($owner->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $owner->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $owner->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($owner->image) {
                $userArr['image'] = URL::to('/') . "/images/profile/$owner->image";
            }
            $myPost['user'] = $userArr;
            $yourRequests[$key]['post'] = $myPost;
        }
        $posts = Post::where('user_id', $user->id)
        ->where('deleted', false)
        ->where('is_waiting', true)
        ->get();
        $yourPostsUpdates = collect(new Post);
        foreach ($posts as $post) {
            $yourPostsUpdate = Applicant::where('post_id', $post->id)->where('seen_by_owner', false)->get();
            Applicant::where('post_id', $post->id)->where('seen_by_owner', false)->update([
                'seen_by_owner' => true
            ]);
            foreach ($yourPostsUpdate as $key => $value) {
                $myPost = Post::find($value->post_id);
                if ($myPost->image) {
                    $myPost['image'] = URL::to('/') . "/images/posts/$value->image";
                }
                if ($myPost->location) {
                    $city = City::find($myPost->location);
                    if ($city) {
                        $myPost['location'] = $city['city'];
                        $country = Country::where('code', $city->iso2)->first();
                        $myPost['country'] = $country['name']; 
                    }
                }
                $applicantsNumber = Applicant::where('post_id', $myPost->id)->get()->count();
                $myPost['number_of_applicants'] = $applicantsNumber; 
                $owner = User::find($myPost->user_id);
                $userArr = [];
                $userType = Account_type::find($owner->account_type_id);
                if ($userType->type == 'normal_user') {
                    $userDetails = User_details::where('user_id', $owner->id)->first();
                    $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
                }
                if ($userType->type == 'company') {
                    $userDetails = Company_details::where('user_id', $owner->id)->first();
                    $userArr['name'] = $userDetails->institute_name;
                }
                if ($owner->image) {
                    $userArr['image'] = URL::to('/') . "/images/profile/$owner->image";
                }
                $myPost['user'] = $userArr;
                $yourPostsUpdate[$key]['post'] = $myPost;
                $yourPostsUpdates->push($yourPostsUpdate[$key]);
            }
        }
        $notifications = [];
        $notifications['your_requests'] = $yourRequests;
        $notifications['your_posts_updates'] = $yourPostsUpdates;
        return response()->json([
            'notifications' => $notifications
        ]);
    }
    public function isUserAcceptedInPost(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
        ]);
        $user = $request->user();
        $post = Post::find($request->post_id);
        $applicant = Applicant::where('user_id', $user->id)
        ->where('post_id', $post->id)
        ->where('application_status', 'accepted')
        ->first();
        if (!$applicant) {
            return response()->json([
                'accepted' => false
            ]);
        }
        return response()->json([
            'accepted' => true
        ]);
    }
    public function UserAppliedToPost(Request $request)
    {
        $request->validate([
            'post_id' => ['required', 'exists:posts,id'],
        ]);
        $user = $request->user();
        $post = Post::find($request->post_id);
        $applicant = Applicant::where('user_id', $user->id)
        ->where('post_id', $post->id)->first();
        if (!$applicant) {
            return response()->json([
                'applied' => false
            ]);
        }
        return response()->json([
            'applied' => true
        ]);
    }
    public function search(Request $request)
    {
        $request->validate([
            'word' => 'required'
        ]);
        $word = $request->word;
        $arr = TagController::findTagOrCategory($word);
        $tags = $arr['tags'];
        $categories = $arr['categories'];


        //posts
        $postsByTitle = DB::table('posts')->where([
            ['title', 'like', "$word%"],
            ['is_waiting', '=', true],
            ['deleted', '=', false],
            ['accepted', '=', true],
        ])->get();

        $postsByTag = collect(new Post);
        foreach ($tags as $tag) {
            $postTags = Post_tag::where('tag_id', $tag->id)->get('post_id');
            foreach ($postTags as $postTag) {
                $postByTag = Post::find($postTag->post_id);
                $postsByTag->push($postByTag);
            }
        }
        foreach ($categories as $category) {
            $postCategories = Post_tag::where('tag_id', $category->id)->get('post_id');
            foreach ($postCategories as $postCategory) {
                $postByTag = Post::find($postCategory->post_id);
                $postsByTag->push($postByTag);
            }
        }


        //users
        $usersByName = collect(new User);
        $userDetails = DB::table('user_details')->where([
            ['first_name', 'like', "$word%"],
        ])->orWhere([
            ['last_name', 'like', "$word%"]
        ])->get('user_id');
        foreach ($userDetails as $oneUserDetails) {
            $userByName = User::find($oneUserDetails->user_id);
            $usersByName->push($userByName);
        }
        $companyDetails = DB::table('company_details')->where([
            ['institute_name', 'like', "$word%"],
        ])->get('user_id');
        foreach ($companyDetails as $oneCompanyDetails) {
            $userByName = User::find($oneCompanyDetails->user_id);
            $usersByName->push($userByName);
        }

        $usersByTag = collect(new User);
        foreach ($tags as $tag) {
            $userTags = User_tag::where('tag_id', $tag->id)->get('user_id');
            foreach ($userTags as $userTag) {
                $userByTag = User::find($userTag->user_id);
                $usersByTag->push($userByTag);
            }
        }
        foreach ($categories as $category) {
            $userCategories = User_tag::where('tag_id', $category->id)->get('user_id');
            foreach ($userCategories as $userCategory) {
                $userByTag = User::find($userCategory->user_id);
                $usersByTag->push($userByTag);
            }
        }


        //merge and delete duplicating
        $posts = $postsByTitle->merge($postsByTag);
        $posts = $posts->unique();
        $users = $usersByName->merge($usersByTag);
        $users = $users->unique();


        //get the rest of posts data
        foreach ($posts as $key => $value) {
            $user = User::find($value->user_id);
            $userArr = [];
            $userType = Account_type::find($user->account_type_id);
            if ($userType->type == 'normal_user') {
                $userDetails = User_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->first_name . ' ' . $userDetails->last_name;
            }
            if ($userType->type == 'company') {
                $userDetails = Company_details::where('user_id', $user->id)->first();
                $userArr['name'] = $userDetails->institute_name;
            }
            if ($user->image) {
                $userArr['image'] = URL::to('/') . "/images/profile/$user->image";
            }
            $posts[$key]['user'] = $userArr;
            $ratings = Post_rating::where('post_id', $value->id)->get();
            $posts[$key]['ratings'] = $ratings;
            if ($value->image) {
                $posts[$key]['image'] = URL::to('/') . "/images/posts/$value->image";
            }
            $city = City::find($value->location);
            if ($city) {
                $posts[$key]['location'] = $city['city'];
                $country = Country::where('code', $city->iso2)->first();
                $posts[$key]['country'] = $country['name']; 
            }
            $posts[$key]['total_rating'] = PostController::getPostTotalRating($value->id);
            $arr = TagController::getPostTagsAndCategories($value->id);
            $posts[$key]['tags'] = $arr['tags'];
            $posts[$key]['categories'] = $arr['categories'];
            $applicantsNumber = Applicant::where('post_id', $value->id)->get()->count();
            $posts[$key]['number_of_applicants'] = $applicantsNumber; 
        }

        //get the rest of users data
        $admins = collect(new User);
        $returnUsers = collect(new User);
        $companies = collect(new User);
        foreach ($users as $key => $value) {
            if (!$value) {
                continue;
            }
            $accountType = Account_type::find($value->account_type_id);
            if ($accountType->type == 'normal_user') {
                $details = User_details::where('user_id', $value->id)->first();
                $users[$key]['user_type'] = 'normal_user';
            }elseif ($accountType->type == 'company') {
                $details = Company_details::where('user_id', $value->id)->first();
                $instituteType = Institute_type::find($details->institute_type_id);
                $details->institute_type = $instituteType->type;
                $numberOfEmployees = Number_of_employees::find($details->number_of_employees);
                $details->number_of_employees = $numberOfEmployees->num_of_emp;
                $users[$key]['user_type'] = 'company';
            }else{ //admin
                $details = null;
                $users[$key]['user_type'] = 'admin';
                $admins->push($users[$key]);
                continue;
            }
            $users[$key]['details'] = $details;
            $arr = TagController::getUserTagsAndCategories($value->id);
            $users[$key]['tags'] = $arr['tags'];
            $users[$key]['categories'] = $arr['categories'];
            $reviews = Review::where('user_2_id', $value->id)->get(['user_1_id as reviewer_id', 'review']);
            $users[$key]['reviews'] = $reviews;
            if ($value->image) {
                $users[$key]['image'] = URL::to('/') . "/images/profile/$value->image";
            }
            if ($value->city_id) {
                $city = City::find($value->city_id);
                $users[$key]['city'] = $city['city'];
            }
            if ($value->country_id) {
                $country = Country::find($value->country_id);
                $users[$key]['country'] = $country['name'];
            }
            $userPosts = Post::where('user_id', $value->id)->where('deleted', false)->get();
            $users[$key]['number_of_posts'] = $userPosts->count();

            if ($accountType->type == 'normal_user') {
                $returnUsers->push($users[$key]);
            }elseif ($accountType->type == 'company') {
                $companies->push($users[$key]);
            }
        }


        return response()->json([
            'posts' => $posts,
            'users' => $returnUsers,
            'companies' => $companies,

        ]);
    }


    private function checkPassword(User $user, string $password): bool
    {
        return Hash::check($password, $user['password']);
    }
    private function checkNormalUser(User $user):bool
    {
        $accountType = Account_type::find($user['account_type_id']);
        return $accountType['type'] == 'normal_user';
    }
    private function checkCompany(User $user):bool
    {
        $accountType = Account_type::find($user['account_type_id']);
        return $accountType['type'] == 'company';
    }
}
