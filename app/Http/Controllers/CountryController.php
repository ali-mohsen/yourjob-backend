<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function getAllCountries(){
        return response()->json([
            Country::get()
        ], 200);
    }
    public function getAllCities(){
        return response()->json([
            City::all()->take(100)
        ], 200);
    }
    public function getCountryCities(Request $request){
        $request->validate([
            'country' => 'required',
        ]);
        $country = Country::where('name', $request->input('country'))->first();
        if (!$country) {
            return response()->json([
                'message' => 'this country is not found'
            ]);
        }
        $cities = City::where('iso2', $country['code'])->get();
        return response()->json([
            $cities
        ], 200);
    }

 
    public function searchCountryByName(Request $request){
        $request->validate([
            'country' => 'required'
        ]);
        $search = $request->input('country');
        $countries = Country::query()->where('name', 'LIKE', "%{$search}%")->get();
        return response()->json([
            'maching countries' => $countries
        ], 200);
    }

    public function searchCityByNameAndCountry(Request $request){
        $request->validate([
            'country' => 'required',
            'city' => 'required'
        ]);
        $search = $request->input('city');
        $request->input('country');
        $country = Country::where('name', $request->input('country'))->first();
        if (!$country) {
            return response()->json([
                'message' => 'this country is not found'
            ]);
        }
        $cities = City::query()->where('iso2', $country->code)->where('city', 'LIKE', "%{$search}%")->get();        
        
        return response()->json([
            'maching cities' => $cities
        ], 200);
    }
}
