<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Post_tag;
use App\Models\Tag;
use App\Models\Tag_type;
use App\Models\User;
use App\Models\User_tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{

    public function suggestTagsOrCategories(Request $request)
    {
        $request->validate([
            'word' => 'required',
            'tag_type' => 'required'
        ]);
        $word = $request->input('word');
        $tagType = $request->input('tag_type');
        $tagType = Tag_type::where('type', $tagType)->first();
        $tagTypeId = $tagType['id'];
        if ($word == '*') {
            $tags = DB::table('tags')->where([
                ['show_in_suggestions', '=', true],
                ['tag_type_id', '=', $tagTypeId],
            ])->get();
        }else{
            $tags = DB::table('tags')->where([
                ['tag', 'like', "$word%"],
                ['show_in_suggestions', '=', true],
                ['tag_type_id', '=', $tagTypeId],
            ])->get();
        }
        $retObg = [];
        foreach ($tags as $key => $value) {
            $retObg[$key] = $value->tag;
        }

        return response()->json([
            'tags' => $retObg
        ], 200);
    }
    public function createTagOrCategory(Request $request)
    {
        $request->validate([
            'tag' => 'required',
            'tag_type' => 'required'
        ]);
        $user =  $request->user();
        if (!$user->tokenCan('admin')) {
            return response()->json([
                'message' => 'you do not have permission to create tags'
            ], 403);
        }
        $newTag = [];
        $newTag['tag'] = $request->input('tag');
        $tagType = Tag_type::where('type', $request->input('tag_type'))->first();
        $newTag['tag_type_id'] = $tagType['id'];
        $newTag['show_in_suggestions'] = true;
        $tagObg = Tag::create($newTag);
        return response()->json([
            'message' => 'Done'
        ],200);
    }

    public static function getPostTagsAndCategories($postId){
        $tags = Post_tag::where('post_id', $postId)->get();
        $tagsArr = array();
        $categoriesArr = array();
        foreach ($tags as $tag) {
            $tagObj = Tag::find($tag->tag_id);
            $tagType = Tag_type::find($tagObj->tag_type_id);
            if ($tagType->type == 'tag') {
                $tagsArr[] = $tagObj->tag;
            } else if ($tagType->type == 'category') {
                $categoriesArr[] = $tagObj->tag;
            }
        }
        return [
            'tags' => $tagsArr,
            'categories' => $categoriesArr,
        ];
    }
    public static function getUserTagsAndCategories($userId){
        $tags = User_tag::where('user_id', $userId)->get();
        $tagsArr = array();
        $categoriesArr = array();
        foreach ($tags as $tag) {
            $tagObj = Tag::find($tag->tag_id);
            $tagType = Tag_type::find($tagObj->tag_type_id);
            if ($tagType->type == 'tag') {
                $tagsArr[] = $tagObj->tag;
            } else if ($tagType->type == 'category') {
                $categoriesArr[] = $tagObj->tag;
            }
        }
        return [
            'tags' => $tagsArr,
            'categories' => $categoriesArr,
        ];
    }

    public static function addTagOrCategoryToUser(User $user, string $tag, $tagType = 'tag'){
        $tagType = Tag_type::where('type', $tagType)->first();
        $tagObg = Tag::where('tag', $tag)->where('tag_type_id', $tagType['id'])->first();
        if ($tagObg == null) {
            $newTag = [];
            $newTag['tag'] = $tag;
            $newTag['tag_type_id'] = $tagType['id'];
            $tagObg = Tag::create($newTag);
        }
        $newUserTag = [];
        $newUserTag['user_id'] = $user['id'];
        $newUserTag['tag_id'] = $tagObg['id'];
        User_tag::create($newUserTag);
    }
    public static function addTagOrCategoryToPost(Post $post, string $tag, $tagType = 'tag'){
        $tagType = Tag_type::where('type', $tagType)->first();
        $tagObg = Tag::where('tag', $tag)->where('tag_type_id', $tagType['id'])->first();
        if ($tagObg == null) {
            $newTag = [];
            $newTag['tag'] = $tag;
            $newTag['tag_type_id'] = $tagType['id'];
            $tagObg = Tag::create($newTag);
        }
        $newPostTag = [];
        $newPostTag['post_id'] = $post['id'];
        $newPostTag['tag_id'] = $tagObg['id'];
        Post_tag::create($newPostTag);
    }
    //if deleteAll true will delete all this user (tags and categories) send tag or category in $tag
    public static function deleteTagOrCategoryOfUser(User $user, string $tag , bool $deleteAll = false)
    {
        if ($deleteAll) {
            if ($tag != 'tag' && $tag != 'category') {
                echo 'wrong at deleteTagOrCategoryOfUser function';
                die;
            }
            $tagType = Tag_type::where('type', $tag)->first();
            $tags = Tag::where('tag_type_id', $tagType['id'])->get();
            foreach ($tags as $tag) {
                DB::table('user_tags')->where([
                    ['user_id', '=', $user['id']],
                    ['tag_id', '=', $tag['id']],
                ])->delete();   
            }
        }else {
            $tagObg = Tag::where('tag', $tag)->first();
            DB::table('user_tags')->where([
                ['user_id', '=', $user['id']],
                ['tag_id', '=', $tagObg['id']],
            ])->delete();
        }
        return response()->json([
            'message' => 'done'
        ], 200);
    }
     //if deleteAll true will delete all this post (tags and categories) send tag or category in $tag
    public static function deleteTagOrCategoryOfPost(Post $post, string $tag , bool $deleteAll = false)
    {
        if ($deleteAll) {
            if ($tag != 'tag' && $tag != 'category') {
                echo 'wrong at deleteTagOrCategoryOfPost function';
                die;
            }
            $tagType = Tag_type::where('type', $tag)->first();
            $tags = Tag::where('tag_type_id', $tagType['id'])->get();
            foreach ($tags as $tag) {
                DB::table('post_tags')->where([
                    ['post_id', '=', $post['id']],
                    ['tag_id', '=', $tag['id']],
                ])->delete();
            }
        }else {
            $tagObg = Tag::where('tag', $tag)->first();
            DB::table('post_tags')->where([
                ['post_id', '=', $post['id']],
                ['tag_id', '=', $tagObg['id']],
            ])->delete();
        }
        return response()->json([
            'message' => 'done'
        ], 200);
    }
    public static function findTagOrCategory($word)
    {
        $tagType = Tag_type::where('type', 'tag')->first();
        $categoryType = Tag_type::where('type', 'category')->first();
        $tags = DB::table('tags')->where([
            ['tag', 'like', "$word%"],
            ['tag_type_id', '=', $tagType->id],
        ])->get();
        $categories = DB::table('tags')->where([
            ['tag', 'like', "$word%"],
            ['tag_type_id', '=', $categoryType->id],
        ])->get();
        $arr['tags'] = $tags;
        $arr['categories'] = $categories;
        return $arr;
    }
}
