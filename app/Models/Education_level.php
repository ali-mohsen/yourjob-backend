<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education_level extends Model
{
    use HasFactory;
    protected $fillable = [
        'institute_name',
        'user_id',
        'institute_type_id',
        'institute_field',
        'about',
        'number_of_employees',
        'verified',
        'verfication',
    ];
}
