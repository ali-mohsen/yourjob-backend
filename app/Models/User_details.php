<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_details extends Model
{
    use HasFactory;

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_details';
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'gender',
        'birth_year',
        'education_level_id',
        'main_profession',
        'work_experience',
        'education',
        'skills'

    ];
}
