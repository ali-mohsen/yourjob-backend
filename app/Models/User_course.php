<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_course extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'course_title',
        'about_the_course',
    ];
}
