<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_package extends Model
{
    use HasFactory;

    protected $table = 'user_packages';
    protected $fillable = [
        'user_id',
        'package_id',
        'still_have_posts',
        'still_available',
    ];

}
