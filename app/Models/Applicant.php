<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    use HasFactory;

    protected $table = 'applicants';

    protected $fillable = [
        'user_id',
        'post_id',
        'application_status',
        'seen_by_owner',
        'seen_by_user',
    ];
}
