<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company_details extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'institute_name',
        'user_id',
        'institute_type_id',
        'institute_field',
        'about',
        'number_of_employees',
        'verified',
        'verfication',
    ];
}
