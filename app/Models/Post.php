<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type_id',
        'title',
        'is_waiting',
        'about',
        'money',
        'image',
        'modified',
        'deleted',
        'accepted',
        'required_experience',
        'employment_type',
        'location',
        'servise_time',
    ];
}
