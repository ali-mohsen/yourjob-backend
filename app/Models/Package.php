<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    protected $table = 'packages';
    public $timestamps = false;
    protected $fillable = [
        'package_for',
        'price',
        'posts_number',
        'availble_for',
    ];
}
