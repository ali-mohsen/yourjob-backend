<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>documentation</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<div class="container"> 
    <div class="row">
        <div class="col-3 order-0" id="sticky-sidebar">
            <div class="sticky-top d-flex flex-column flex-shrink-0 p-3 text-white bg-dark">
                <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                <span class="fs-4">Documentation:</span>
                </a>
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">
                        Getting Started
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#createAccount">Create user | company Account</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#createAdminAccount">Create admin Account</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#sigin">Signin</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#updateUser">Update User Account</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#deleteUser">Delete User Account</a>
                    </li>


                    
                    
                </ul>
            </div>
        </div>
        
        <div class="col-9 py-3" id="main">
            <h2>the base URl:</h2>
            <p class="link">YOUR_HOST:8000/api</p>
            <p>notice that you have to send a token with some APIs than need authorisation.
            you will get this toke by signing a user in.
            then you send it like this: </p>
            <p>to start the server type the folowing in cmd:</p>
            <p style="color: darkred;">php artisan serve</p>
 <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="one-API">
                <h2 id="createAccount">create normal user or company account API:</h2>
                <p>type: post</p>
                <h5>link</h5>
                <p class="link">/register</p>
                <h5>json data you send</h5>
                <div class="jsondata">

    <pre>
    {
        email: UNIQUE EMAIL,
        password: USERPASSWORD.
        account_type: NORMAL_USER | COMPANY,
        phone_number: NUMBER,
        country: COUNTRY NAME,
        city: CITY NAME,
        image: JPEG | PNG | JPG,
        <small >for normal user</small>
        first_name: FIRST NAME,
        last_name: LAST NAME,
        gender: GENDER,
        education_level: EDUATION LEVEL NAME,
        languages: ARRAY OF LANGUAGE NAMES,
        courses: ARRAY OF COURSES,
        <small>optional</small>
        birth_year: BIRTH YEAR,
        main_profession: MAIN PROFITION,
        work_experience: WORK experience,
        education: EDUCATION,
        skills: NEW SKILLS,
        <small>for company</small>
        institute_name: COMPANY NAME,
        verification: VALIDATE IMAGE JPEG | PNG | JPG,
        institute_type: INSTITUTE TYPE NAME
        number_of_employees: ONE OF THE FOLLOWING (1-10, 11-50 ,51-100, 100-500, 500-1000, 1000+),
        institute_field: COMPANY FIELD,
        <small>optional</small>
        about: ABOUT COMPANY
    }
    </pre>
                
                </div>
                
                <h5>json data you get on sucsess</h5>
                <div class="jsondata">
    <pre>
    {
        created user: THE NEW USER,
        details: USER DETAILS
    }
    </pre>
                </div>
                <small>do not need an authenticated user</small>
            </div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="one-API">
                <h2 id="createAdminAccount">create admin account API:</h2>
                <p>type: post</p>
                <h5>link</h5>
                <p class="link">/register-admin</p>
                <h5>json data you send</h5>
                <div class="jsondata">
    <pre>
    {
        email: UNIQUE EMAIL,
        password: PASSWORD,
        dashboared_password: THE SECRET SECRET PASSWORD (123),
    }
    </pre>
                
                </div>
                
                <h5>json data you get on sucsess</h5>
                <div class="jsondata">
    <pre>
    {
        'user' => THE CREATE ADMIN ACCOUNT,
    }
    </pre>
                </div>
                <small>do not need an authenticated user</small>
            </div>
 <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="one-API">
                <h2 id="sigin">sign a user in API:</h2>
                <p>type: post</p>
                <h5>link</h5>
                <p class="link">/login</p>
                <h5>json data you send</h5>
                <div class="jsondata">
                    
    <pre>
    {
        email: USER EMAIL,
        password: USER PASSWORD
    }
    </pre>
                
                </div>
                
                <h5>json data you get on sucsess</h5>
                <div class="jsondata">
    <pre>
    {
        'user' => THE USER WHO SIGNED IN,
        'token' => THE TOKEN THIS USER SHOULD USE
    }
    </pre>
                </div>
                <small>do not need an authenticated user</small>
            </div>
 <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


 <div class="one-API">
                <h2 id="updateUser">Update a user data:</h2>
                <p>type: post</p>
                <h5>link</h5>
                <p class="link">/update-this-account</p>
                <h5>json data you send</h5>
                <div class="jsondata">
                    
    <pre>
    {
        password: USER PASSWORD,
        <small>the reast is optional</small>

        new_password: USER NEW PASSWORD TO BE UPDATED,
        new_image: A NEW IMAGE TO BE UPDATED,
        new_phone_number: NEW NUMBER TO REPLACE THE OLD ONE,
        new_country: NEW COUNTRY TO REPLACE THE OLD  ONE,
        new_city: NEW CITY TO REPLACE THE OLD ONE
        <small >for normal user</small>
        new_birth_year: NEW BIRTH YEAR,
        new_first_name: NEW FIRST NAME,
        new_last_name: NEW LAST NAME,
        new_gender: GENDER TO REPLACE THE OLD ONE,
        new_main_profession: NEW  MAIN PROFITION TO REPLACE THE OLD ONE,
        new_work_experience: NEW WORK experience,
        new_education: NEW EDUCATION TO REPLACE THE OLD ONE,
        new_skills: NEW SKILLS TO REPLCAE THE OLD ONE,
        new_education_level: NEW EDUCATION LEVEL TO REPLACE THE OLD ONE,
        <small>for company</small>
        new_institute_type: NEW COMPANY TYPE,
        new_institute_name: NEW NAME,
        new_institute_field: NEW FIELDS,
        new_about: NEW ABOUT,
        new_number_of_employees: NEW NUMBER OF EMPLOYEES,
    }
    </pre>
                
                </div>
                
                <h5>json data you get on sucsess</h5>
                <div class="jsondata">
    <pre>
    {
        'user' => THE USER UPDATED,
        'details' => DETAILS ABOUT THIS USER
    }
    </pre>
                </div>
                <small>do not need an authenticated user</small>
            </div>
 <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


 <div class="one-API">
                <h2 id="deleteUser">Delete user account:</h2>
                <p>type: post</p>
                <h5>link</h5>
                <p class="link">/delete-this-account</p>
                <h5>json data you send</h5>
                <div class="jsondata">
                    
    <pre>
    {
        password: USER PASSWORD
    }
    </pre>
                
                </div>
                
                <h5>json data you get on sucsess</h5>
                <div class="jsondata">
    <pre>
    {
        'message' => 'deleted'
    }
    </pre>
                </div>
                <small>do not need an authenticated user</small>
            </div>
 <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


        </div>

    </div>
</div>

<style>
    pre > small {
        color: red;
    }
    .link{
        color: #5a5aec;
        font-weight: bold;
    }
    .one-API{
        background-color: #e8e8bc;
        border-radius: 50px;
        padding: 10px 0;
        padding-inline-start: 50px;
        margin-bottom: 30px;
    }
    .sticky-top{
        height: 100vh;
    }
    .container{
        margin-left: 0;
        padding-left: 0;
    }
    .nav-link{
        color: white;
    }
</style>

<script>
    $(document).ready(function () {
        $('.nav li a').click(function(e) {
            $('.nav li a.active').removeClass('active');
            $(this).addClass('active');
            //e.preventDefault();
        });
    });
</script>

</body>
</html>